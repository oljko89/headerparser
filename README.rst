Test task Repository
========================

This simple project which gives user possibility to count void functions in
[C language](https://en.wikipedia.org/wiki/C_(programming_language)) header file.
To run script use `python <script_name> <path_to_file>`