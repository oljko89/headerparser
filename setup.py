

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme_content = readme_file.read()

setup(
    name='parser_utils',
    version='0.1.0',
    description='Sample program to parse header files',
    long_description=readme_content,
    author='Oleh Melnyk',
    author_email='olegmelnyk52@gmail.com',
    packages=find_packages(exclude=('test', 'docs'))
)
