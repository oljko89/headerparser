from unittest import TestCase, TestSuite, makeSuite
from unittest.mock import patch, mock_open

from parser_utils import header_parser


class TestHeaderParser(TestCase):

    path_to_header_file = '/home/user/dir/header_file.h'

    def test_001_get_count_of_void_functions(self):
        """
        Test 001: get_count_of_void_functions.
        Input is not empty, one void functions exists.
        """
        result = header_parser.\
            get_count_of_void_functions('static void wrapByteArrayProperty'
                                        '(const string &type, ByteArray'
                                        '*input);\nstatic list<string>'
                                        'updateCardDatabases();')
        expected_result = 1
        self.assertEqual(result, expected_result)

    def test_002_get_count_of_void_functions(self):
        """
        Test 002: get_count_of_void_functions.
        Input is empty.
        """
        result = header_parser.get_count_of_void_functions('')
        expected_result = 0
        self.assertEqual(result, expected_result)

    def test_003_get_count_of_void_functions(self):
        """
        Test 003: get_count_of_void_functions.
        Input is not empty, void functions doesn't exists.
        """
        result = header_parser.\
            get_count_of_void_functions('static bool wrapByteArrayProperty'
                                        '(const string &type, ByteArray'
                                        '*input);\nstatic list<string> '
                                        'updateCardDatabases();')
        expected_result = 0
        self.assertEqual(result, expected_result)

    def test_004_get_count_of_void_functions(self):
        """
        Test 004: get_count_of_void_functions.
        Input is not empty, one void function without arguments.
        """
        result = header_parser.\
            get_count_of_void_functions('static bool wrapByteArrayProperty'
                                        '(const string &type, ByteArray'
                                        '*input);\n'
                                        'void wrapByteArrayProperty'
                                        '(void);')
        expected_result = 1
        self.assertEqual(result, expected_result)

    def test_005_get_count_of_void_functions(self):
        """
        Test 005: get_count_of_void_functions.
        Input is not empty, one virtual void function without arguments.
        """
        result = header_parser.\
            get_count_of_void_functions('#include "db.h"\n'
                                        '#define SECURE_VALUE_PROPERTY_POSTFIX'
                                        ' "_secureValue"\n'
                                        'using std::list;\n'
                                        'class HCEProperties\n'
                                        '{public:\n'
                                        'virtual void wrapByteArrayProperty'
                                        '(void);'
                                        '};')
        expected_result = 1
        self.assertEqual(result, expected_result)

    def test_006_get_count_of_void_functions(self):
        """
        Test 006: get_count_of_void_functions.
        Input is not empty, one void function, new line after void.
        """
        result = header_parser.\
            get_count_of_void_functions('#include "db.h"\n'
                                        '#define SECURE_VALUE_PROPERTY_POSTFIX'
                                        ' "_secureValue"\n'
                                        'using std::list;\n'
                                        'class HCEProperties\n'
                                        '{public:\n'
                                        'void\nwrapByteArrayProperty();'
                                        '};')
        expected_result = 1
        self.assertEqual(result, expected_result)

    def test_007_get_count_of_void_functions(self):
        """
        Test 007: get_count_of_void_functions.
        Input is not empty, one void function, two new lines after void.
        """
        result = header_parser.\
            get_count_of_void_functions('#include "db.h"\n'
                                        '#define SECURE_VALUE_PROPERTY_POSTFIX'
                                        ' "_secureValue"\n'
                                        'using std::list;\n'
                                        'class HCEProperties\n'
                                        '{public:\n'
                                        'void\n\nwrapByteArrayProperty();'
                                        '};')
        expected_result = 1
        self.assertEqual(result, expected_result)

    def test_008_get_count_of_void_functions(self):
        """
        Test 008: get_count_of_void_functions.
        Input is not empty, one void function, empty body.
        """
        result = header_parser.\
            get_count_of_void_functions('#include "db.h"\n'
                                        '#define SECURE_VALUE_PROPERTY_POSTFIX'
                                        ' "_secureValue"\n'
                                        'using std::list;\n'
                                        'class HCEProperties\n'
                                        '{public:\n'
                                        'void wrapByteArrayProperty(){};'
                                        '};')
        expected_result = 1
        self.assertEqual(result, expected_result)

    def test_009_get_count_of_void_functions(self):
        """
        Test 009: get_count_of_void_functions.
        Input is not empty, one void function with implementation.
        """
        result = header_parser.\
            get_count_of_void_functions('#include "db.h"\n'
                                        '#define SECURE_VALUE_PROPERTY_POSTFIX'
                                        ' "_secureValue"\n'
                                        'using std::list;\n'
                                        'class HCEProperties\n'
                                        '{public:\n'
                                        'void wrapByteArrayProperty(){\n'
                                        '   static i = 0;\n};'
                                        '};')
        expected_result = 1
        self.assertEqual(result, expected_result)

    def test_010_get_count_of_void_functions(self):
        """
        Test 010: get_count_of_void_functions.
        Input is not empty, one void function,
        'void' word in function name.
        """
        result = header_parser.\
            get_count_of_void_functions('#include "db.h"\n'
                                        '#define SECURE_VALUE_PROPERTY_POSTFIX'
                                        ' "_secureValue"\n'
                                        'using std::list;\n'
                                        'class HCEProperties\n'
                                        '{public:\n'
                                        'void voidFunction(int b);'
                                        '};')
        expected_result = 1
        self.assertEqual(result, expected_result)

    def test_011_get_count_of_void_functions(self):
        """
        Test 011: get_count_of_void_functions.
        Input is not empty, returns void pointer.
        """
        result = header_parser.\
            get_count_of_void_functions('#include "db.h"\n'
                                        '#define SECURE_VALUE_PROPERTY_POSTFIX'
                                        ' "_secureValue"\n'
                                        'using std::list;\n'
                                        'class HCEProperties\n'
                                        '{public:\n'
                                        'void* function();'
                                        '};')
        expected_result = 0
        self.assertEqual(result, expected_result)

    def test_012_get_count_of_void_functions(self):
        """
        Test 012: get_count_of_void_functions.
        Input is not empty, void pointer.
        """
        result = header_parser.\
            get_count_of_void_functions('#include "db.h"\n'
                                        '#define SECURE_VALUE_PROPERTY_POSTFIX'
                                        ' "_secureValue"\n'
                                        'using std::list;\n'
                                        'class HCEProperties\n'
                                        '{public:\n'
                                        'void* _mPtr;'
                                        '};')
        expected_result = 0
        self.assertEqual(result, expected_result)

    def test_013_get_count_of_void_functions(self):
        """
        Test 013: get_count_of_void_functions.
        Input is not empty, 8 void functions.
        """
        result = header_parser.\
            get_count_of_void_functions('#include "db.h"\n'
                                        '#define SECURE_VALUE_PROPERTY_POSTFIX'
                                        ' "_secureValue"\n'
                                        'using std::list;\n'
                                        'class HCEProperties\n'
                                        '{public:\n'
                                        'void* func1();'
                                        'void func2(void);'
                                        'static void func3();'
                                        'virtual void func4();'
                                        'void\n'
                                        '    func9(void);'
                                        'void\n'
                                        '\n'
                                        '   func10(void);'
                                        'void func11() {};'
                                        'void func12() {\n'
                                        '   static int i = 0;\n'
                                        '};'
                                        'void voidFunc(int a);'
                                        'int voidFunc(int b);'
                                        'void* _mPtr;'
                                        '};')
        expected_result = 8
        self.assertEqual(result, expected_result)

    @patch('builtins.open', new_callable=mock_open())
    @patch('parser_utils.header_parser.os.path.isfile')
    def test_014_read_header_file(self, mock_is_file, mock_open_file):
        """
        Test 014: read_header_file.
        File exists, file was read.
        """
        mock_is_file.return_value = True
        header_parser.read_header_file(self.path_to_header_file)
        mock_open_file.assert_called_with(self.path_to_header_file)
        mock_is_file.assert_called_with(self.path_to_header_file)

    @patch('logging.error')
    @patch('builtins.open', new_callable=mock_open())
    @patch('parser_utils.header_parser.os.path.isfile')
    def test_015_read_header_file(self, mock_is_file,
                                  mock_open_file,
                                  mock_logging):
        """
        Test 015: read_header_file.
        File exists, file wasn't read.
        """
        logging_message = 'Can not read file: {}'.\
            format(self.path_to_header_file)
        mock_is_file.return_value = True
        mock_open_file.side_effect = IOError()
        header_parser.read_header_file(self.path_to_header_file)
        mock_open_file.assert_called_with(self.path_to_header_file)
        mock_logging.assert_called_with(logging_message)

    @patch('logging.error')
    @patch('parser_utils.header_parser.os.path.isfile')
    def test_016_read_header_file(self, mock_is_file, mock_logging):
        """
        Test 016: read_header_file.
        File doesn't exists, file was not read.
        """
        logging_message = 'File {} does not exist'.\
            format(self.path_to_header_file)
        mock_is_file.return_value = False

        header_parser.read_header_file(self.path_to_header_file)
        mock_is_file.assert_called_with(self.path_to_header_file)
        mock_logging.assert_called_with(logging_message)


def create_test_suite():
    suite = TestSuite()
    suite.addTest(makeSuite(TestHeaderParser))
    return suite
