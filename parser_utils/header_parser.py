
import os
import sys
import re
import logging


def main():
    """
    This is entry point function of this program.
    """
    if len(sys.argv) > 1:
        content = read_header_file(sys.argv[1])
        if content:
            count_of_functions = get_count_of_void_functions(content)
            print('Found {} functions'.format(count_of_functions))
    else:
        logging.error('Please specify file path')


def read_header_file(header_file_path):
    """
    This function read header file and returns it content.

    :param header_file_path: path to header file
    :return: content of header file
    """
    content = ''
    if os.path.isfile(header_file_path):
        try:
            with open(header_file_path) as f:
                content = f.read()
        except Exception as e:
            logging.error('Can not read file: {}'.format(header_file_path))
    else:
        logging.error('File {} does not exist'.format(header_file_path))
    return content


def get_count_of_void_functions(header_file_content):
    """
    This function parse string and finds all void functions.

    :param header_file_content: content of header file to parse
    :return: count of functions which doesn't return anything
    """
    result = re.findall('[^(]void[^\*\w]', header_file_content)
    return len(result)


if __name__ == '__main__':
    main()
